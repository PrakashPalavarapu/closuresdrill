const counterFactory = require("../counterFactory.cjs");

let countObject = counterFactory();
console.log(countObject.incrementCounter());
console.log(countObject.incrementCounter());
console.log(countObject.incrementCounter());
console.log(countObject.decrementCounter());
console.log(countObject.decrementCounter());
console.log(countObject.decrementCounter());
console.log(countObject.incrementCounter());
