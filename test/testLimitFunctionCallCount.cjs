const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

let limit = 10;

let invoker = limitFunctionCallCount((countArg) => {
  return --countArg;
}, limit);

for (let count = 0; count < limit + 5; count++) {
  if (typeof invoker === "function") {
    console.log(invoker());
  } else {
    console.log(invoker);
  }
}
