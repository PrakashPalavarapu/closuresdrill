const cacheFunction = require("../cacheFunction.cjs");

const invoker = cacheFunction((cacheObj, args) => {
  cacheObj[args] = 0;
  console.log("callback invoked");
  return cacheObj[args];
});
console.log(invoker(["a", "b"]));
console.log(invoker(["a", "b"]));
console.log(invoker(["a", "b", "c"]));
console.log(invoker("a"));
console.log(invoker(["a", "b"]));
console.log(invoker(["a", "b"]));
console.log(invoker("b"));
console.log(invoker("b"));
console.log(invoker("c"));
console.log(invoker("c"));
console.log(invoker(""));
console.log(invoker());
