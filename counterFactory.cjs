function counterFactory() {
  let counter = 0;
  counterObject = {
    incrementCounter: () => {
      counter++;
      return counter;
    },
    decrementCounter: () => {
      counter--;
      return counter;
    },
  };
  return counterObject;
}

module.exports = counterFactory;
