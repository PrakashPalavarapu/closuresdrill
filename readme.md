# Insights of closures drill

## CounterFactory:
This function returns an object having two methods, one for increment and other for decrement. The count start from 0, and evert time obj's increment function is called count increases by one and every time obj's decrement is called, the count is decreased by one.

### testing of CounterFactory:
Tested by calling increment and decrement functions multiple times randomly and observed the count is changing according to the requirements of question

## LimitFunctionCallCount:
this function allows user to invoke callback for a specified limit. every time callback is called, it returns number of invokes left. If invokes count exceed limit then function returns null

### Testing and Error Handiling  LimitFunctionCallCount:
Tested by calling the callback more than the specified limit using for loop in test file, and after exceeding limit the function returns null. If other than function type is passed in main function,it returns 'wrong input'

## cacheFunction:
this function keeps the track of args passed into invoker function in an object 'cache'. every time a new argument passed, test file prints "call back invoked" , and for every argument passed to invoker function returns the number of times this arg was passed preveously, this is to ensure that every argument is being tracked in object cache.

### Testing and Error Handiling  cacheFunction:
Tested by passing different args, including passing no args was also considered. Each time new arg is passed test file logs "callback invoked". Check the test file to see the different types of args tested. 
 