function cacheFunction(callBack) {
  const cache = {};
  return function invoker(...args) {
    if (typeof callBack === "function") {
      if (cache[String(...args)] >= 0) {
        cache[String(...args)] += 1;
        return cache[String(...args)];
      } else {
        return callBack(cache, String(...args));
      }
    }
    return `wrong input`;
  };
}
module.exports = cacheFunction;
