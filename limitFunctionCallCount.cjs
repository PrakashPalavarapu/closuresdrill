function limitFunctionCallCount(callBack, n) {
  if (typeof callBack === "function" && typeof n === "number") {
    let count = n;
    return function invoker() {
      if (count != 0) {
        count = callBack(count);
        return `number of invokes reamining for call Back are ${count}`;
      }
      return null;
    };
  }
  return `wrong type of input`;
}

module.exports = limitFunctionCallCount;
